self.addEventListener('push', ev => {
  console.log(self, ev)
  if(!(self.Notification && 'granted' === self.Notification.permission)) {
    return
  }

  const
    sendNotification = body => {
      const
        title = 'Web Push example'

      return self.registration.showNotification(title, { body })
    }

  if(ev.data) {
    const
      msg = ev.data.text()

    ev.waitUntil(sendNotification(msg))
  }
})
